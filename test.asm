section .data
input: db '20'
codes: db '0123456789abcdef'
section .text
global _start

string_length:
    mov rax, rdi
    .counter:
    cmp byte [rdi], 0
    je .end
    inc rdi
    jmp .counter
    .end:
    sub rdi, rax
    mov rax, rdi
    ret

print_string:
	push rdi
	call string_length
	pop rdi
	mov rdx, rax
    mov rsi, rdi
    mov rax, 1
    mov rdi, 1
    syscall
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
print_uint:
        push rbx
        mov r8, rsp
        mov rax, rdi
        mov rbx, 10
        push 0x0
        ;sub rsp, 9
                ;mov r8, rsp
        .cyc:
                xor rdx, rdx
                div rbx
                add dl, '0'
                ;dec rsp
                ;mov byte [rsp], dl
                ;inc rsp
                push rdx
                test rax, rax
                jnz .cyc

        .end:
                                ;mov byte [rsp], 0
                mov rdi, rsp
                                push r8
                call print_string
                                pop r8
                                ;восстановление rsp
                                ;mov r9, rsp
                                ;sub r9, r8
                                ;mov rbx, 9
                                ;sub rbx, r9
                                ;add rsp, rbx
                                sub r8, rsp
                                add rsp, r8
                ;pop rbx ; pop null terminator address
                ;sub r8, rsp
                ;add rsp, r8
                                pop rbx
                ret

print_rax:
	mov rax, rdi
	mov rdi, 1
	mov rdx, 1
	mov rcx, 64
	

	.loop:
	push rax
	sub rcx, 4
	sar rax, cl
	and rax, 0xf
	lea rsi, [codes + rax]
	mov rax, 1

	push rcx
	syscall
	pop rcx

	pop rax
	test rcx, rcx
	jnz .loop

	ret


_start:
	mov rdi, input
	call print_uint

	xor rdi, rax
	mov rax, 60
	syscall
